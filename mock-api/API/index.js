const express = require('express')
const cors = require('cors')
const fs = require('fs')

const app = express();
app.use(cors())

app.use(express.json());


app.get('/Camisetas', (req, res) => {
    const produto =  fs.readFileSync('./data/V1/categories/1.json', 'utf-8')
    res.send(JSON.parse(produto))
})

app.get('/Calcas', (req, res) => {
    const produto =  fs.readFileSync('./data/V1/categories/2.json', 'utf-8')
    res.send(JSON.parse(produto))
})

app.get('/Sapatos', (req, res) => {
    const produto =  fs.readFileSync('./data/V1/categories/3.json', 'utf-8')
    res.send(JSON.parse(produto))
})

app.get('/Menu', (req, res) => {
    const produto =  fs.readFileSync('./data/V1/categories/list.json', 'utf-8')
    res.send(JSON.parse(produto))
})
 
app.listen(3000, function () {
  console.log('Servidor Rodando')
})