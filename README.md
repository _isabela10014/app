## Tecnologias utilizadas 
- Angular 10
- Sass
- node.js
- express

## Front-End
- O projeto foi desenvolvido em Angular na versão 10
- Para executar o projeto é necessário ter o angular instalado em sua maquina
- O front end do projeto esta disponivel em  https://bitbucket.org/_isabela10014/my-dream-app/src/master/
- Ao realizar o download colocar na pasta public e em seguida fazer a instalação do node npm
- Em seu navegado abra o caminho http://localhost:4200/


## Back-End
- Foi utilizado node.js e express
- A api se encontra-se na pasta mock-api/API
- Para executa-la abra o seu terminal e digite "node index.js"
